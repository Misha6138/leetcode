package app;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.swing.tree.TreeNode;
import java.lang.reflect.Array;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MethodsForTest {




    static class Path{
        int x;
        int y;
        Path prev;

        public Path(int x, int y, Path prev){
            this.x = x;
            this.y = y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Path path = (Path) o;
            return x == path.x &&
                    y == path.y &&
                    Objects.equals(prev, path.prev);
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y, prev);
        }
    }



    public static void main(String[] args) {


//        System.out.println(isPathCrossing("NES"));
//
//        System.out.println(isPathCrossing("NESWW"));

        System.out.println(isPathCrossing("WWEEE"));



    }

    public static boolean isPathCrossing(String path) {
        LinkedList<Path> visited = new LinkedList<>();
        visited.add(new Path(0,0, null));
        for(char c : path.toCharArray()){
            if(c == 'N'){
                Path last = visited.getLast();
                if(visited.contains(new Path(last.x, last.y - 1, last))){
                    return true;
                }
                visited.addLast(new Path(last.x, last.y - 1, last));
            }
            if(c == 'W'){
                Path last = visited.getLast();
                if(visited.contains(new Path(last.x - 1, last.y, last))){
                    return true;
                }
                visited.addLast(new Path(last.x - 1, last.y, last));
            }
            if(c == 'S'){
                Path last = visited.getLast();
                if(visited.contains(new Path(last.x, last.y + 1, last))){
                    return true;
                }
                visited.addLast(new Path(last.x, last.y + 1, last));

            }
            if(c == 'E'){
                Path last = visited.getLast();
                if(visited.contains(new Path(last.x + 1, last.y , last))){
                    return true;
                }
                visited.addLast(new Path(last.x + 1, last.y , last));

            }
        }


        return false;

    }



    /*
        Given an array of integers where 1 ≤ a[i] ≤ n (n = size of array), some elements appear twice and others appear once.

        Find all the elements of [1, n] inclusive that do not appear in this array.

        Could you do it without extra space and in O(n) runtime? You may assume the returned list does not count as extra space.
     */

    public static List<Integer> findDisappearedNumbers(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for(int i = 0; i < nums.length; i++){
            set.add(nums[i]);
        }
        List<Integer> list = new ArrayList<>();
        for(int i = 1; i <= nums.length; i++){
            if(!set.contains(i)){
                list.add(i);
            }
        }

        return list;


    }






    /*
        Roman numerals are represented by seven different symbols: I, V, X, L, C, D and M.

        Symbol       Value
        I             1
        V             5
        X             10
        L             50
        C             100
        D             500
        M             1000
        For example, 2 is written as II in Roman numeral, just two one's added together. 12 is written as XII, which is simply X + II. The number 27 is written as XXVII, which is XX + V + II.

        Roman numerals are usually written largest to smallest from left to right. However, the numeral for four is not IIII. Instead, the number four is written as IV. Because the one is before the five we subtract it making four. The same principle applies to the number nine, which is written as IX. There are six instances where subtraction is used:

        I can be placed before V (5) and X (10) to make 4 and 9.
        X can be placed before L (50) and C (100) to make 40 and 90.
        C can be placed before D (500) and M (1000) to make 400 and 900.
        Given a roman numeral, convert it to an integer.
     */

    public static int romanToInt(String s) {
        Map<Character,Integer> map = new HashMap(){{
            put('I', 1);
            put('V', 5);
            put('X', 10);
            put('L', 50);
            put('C', 100);
            put('D', 500);
            put('M', 1000);
        }};
        List<String> list = new ArrayList<>();
        if(s.length() == 1){
            return map.get(s.charAt(0));
        }
        char[] a = s.toCharArray();
        int f = 0;
        int d = 0;
        for(int i = 1; i < a.length; i++){
           if(a[i - 1] == 'I' && (a[i] == 'V' || a[i] == 'X')){
               f =  map.get(a[i]);
               d = map.get(a[i - 1]);
               list.add(String.valueOf(f - d));
               i++;

           }
           else if(a[i - 1] == 'X' && (a[i] == 'L' || a[i] == 'C')){
               f =  map.get(a[i]);
               d = map.get(a[i - 1]);
               list.add(String.valueOf(f - d));
               i++;


           }
           else if(a[i - 1] == 'C' && (a[i] == 'D' || a[i] == 'M')){
               f =  map.get(a[i]);
               d = map.get(a[i - 1]);
               list.add(String.valueOf(f - d));
               i++;
           }else {
               list.add(String.valueOf(map.get(a[i - 1])));
           }
           if(i == a.length - 1){
               list.add(String.valueOf(map.get(a[i])));
           }

        }
        return list.stream().mapToInt(Integer::parseInt).reduce(0, Integer::sum);

    }







    }







