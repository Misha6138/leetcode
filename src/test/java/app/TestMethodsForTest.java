package app;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.junit.jupiter.params.provider.Arguments;

import java.util.List;
import java.util.stream.Stream;

public class TestMethodsForTest {


    private static Stream<Arguments> testFindDisappearedNumbers(){
        return Stream.of(
                Arguments.arguments(new int[]{4,3,2,7,8,2,3,1}, List.of(5,6)),
                Arguments.arguments(new int[]{1,2,3,3,5}, List.of(4))

        );


    }

    @ParameterizedTest
    @MethodSource("testFindDisappearedNumbers")
    void testFindDisappearedNumbers(int [] input, List<Integer> expectedList){
        List<Integer> actual = MethodsForTest.findDisappearedNumbers(input);
        Assertions.assertEquals(actual, expectedList);
    }





    private static Stream<Arguments> testRomanToInt(){
        return Stream.of(
                Arguments.arguments("MCMXCIV", 1994),
                Arguments.arguments("LVIII", 58),
                Arguments.arguments("IX", 9)
        );


    }

    @ParameterizedTest
    @MethodSource("testRomanToInt")
    void testNumIdenticalPairs(String input, int expectedCount){
        int actual = MethodsForTest.romanToInt(input);
        Assertions.assertEquals(actual, expectedCount);
    }









}
